package main

import (
	"systao.fr/gorest/pkg/gorest"

	"github.com/gin-gonic/gin"
	"systao.fr/examples/users/internal/controllers"
	"systao.fr/examples/users/internal/repository"
)

var userRepository = repository.NewUserRepository(repository.InMemory)

var GORestConfig = gorest.Config{
	Controllers: []gorest.HTTPController{
		controllers.NewUserController(userRepository),
	},
	Middlewares: []gin.HandlerFunc{
		gorest.Tracer,
	},
	Host: "localhost",
	Port: 8080,
}
