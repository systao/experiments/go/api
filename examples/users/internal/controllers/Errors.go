package controllers

import "fmt"

type ValidationError struct {
	message string
}

func (e *ValidationError) Error() string {
	return fmt.Sprintf("Validation error: %s", e.message)
}
