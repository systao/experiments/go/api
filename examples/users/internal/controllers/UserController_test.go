package controllers

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"systao.fr/examples/users/internal/models"
	"systao.fr/examples/users/internal/repository"
	"systao.fr/gorest/pkg/gorest"
)

var userId = "9f2b8cf1-7f42-4d9c-82e1-177f193ca8b5"

// mock UserRepository
type userRepositoryMock struct {
	mock.Mock
}

func returns(r *mock.Mock, ret mock.Arguments) (*models.User, error) {
	var r0 *models.User
	if ret.Get(0) != nil {
		r0 = ret.Get(0).(*models.User)
	}

	var r1 error
	if ret.Get(1) != nil {
		r1 = ret.Get(1).(error)
	}

	return r0, r1
}

func (r *userRepositoryMock) FindById(id string) (*models.User, error) {
	return returns(&r.Mock, r.Called(id))

}

func (r *userRepositoryMock) Save(u models.User) (*models.User, error) {
	return returns(&r.Mock, r.Called(u))
}

func serverConfig() (*userRepositoryMock, gorest.Config) {
	gin.SetMode(gin.TestMode)
	var userRepository = new(userRepositoryMock)
	return userRepository, gorest.Config{
		Controllers: []gorest.HTTPController{
			NewUserController(userRepository),
		},
	}
}

type applicationError struct {
	Message string `json:"error"`
}

func TestGreeetings(t *testing.T) {
	t.Run("Simple users", func(t *testing.T) {
		_, config := serverConfig()
		server := gorest.NewServer(config)

		rr := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, "/users/greet/John", bytes.NewBuffer(nil))

		server.Serve(rr, req)

		assert.Equal(t, rr.Code, 200)
		assert.Equal(t, rr.Body.String(), "Hello John")

	})
}

func TestGetUser(t *testing.T) {
	t.Run("Retrieve existing user", func(t *testing.T) {
		userRepository, config := serverConfig()
		server := gorest.NewServer(config)

		userRepository.Mock.On("FindById", userId).Return(&models.User{ID: userId, Name: "John"}, nil)

		rr := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/users/%s", userId), bytes.NewBuffer(nil))

		server.Serve(rr, req)

		user := models.User{}
		json.NewDecoder(rr.Body).Decode(&user)

		assert.Equal(t, 200, rr.Code)
		assert.Equal(t, userId, user.ID)
		assert.Equal(t, "John", user.Name)
	})

	t.Run("Lookup a non existing user", func(t *testing.T) {
		userRepository, config := serverConfig()
		server := gorest.NewServer(config)

		err := &repository.UserNotFoundError{ID: userId}
		userRepository.On("FindById", userId).Return(nil, err)

		rr := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/users/%s", userId), bytes.NewBuffer(nil))

		server.Serve(rr, req)

		error := applicationError{}
		json.NewDecoder(rr.Body).Decode(&error)

		assert.Equal(t, 404, rr.Code)
		assert.Equal(t, fmt.Sprintf("User not found %s", userId), error.Message)
	})

	t.Run("Internal error", func(t *testing.T) {
		userRepository, config := serverConfig()
		server := gorest.NewServer(config)

		err := errors.New("Triggered internal error")
		userRepository.On("FindById", userId).Return(nil, err)

		rr := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/users/%s", userId), bytes.NewBuffer(nil))

		server.Serve(rr, req)

		error := applicationError{}
		json.NewDecoder(rr.Body).Decode(&error)

		assert.Equal(t, 500, rr.Code)
		assert.Equal(t, "Triggered internal error", error.Message)
	})
}

func TestSaveUser(t *testing.T) {
	t.Run("Create new user", func(t *testing.T) {
		userRepository, config := serverConfig()
		server := gorest.NewServer(config)

		userRepository.Mock.On("Save", models.User{Name: "John"}).Return(&models.User{ID: userId, Name: "John"}, nil)

		rr := httptest.NewRecorder()
		data, _ := json.Marshal(&models.User{Name: "John"})
		req, _ := http.NewRequest(http.MethodPost, "/users", bytes.NewBuffer(data))

		server.Serve(rr, req)

		user := models.User{}
		json.NewDecoder(rr.Body).Decode(&user)

		assert.Equal(t, 200, rr.Code)
		assert.Equal(t, userId, user.ID)
		assert.Equal(t, "John", user.Name)
	})

	t.Run("Update existing user", func(t *testing.T) {
		userRepository, config := serverConfig()
		server := gorest.NewServer(config)

		userRepository.Mock.On("Save", models.User{Name: "John", ID: userId}).Return(&models.User{Name: "John", ID: userId}, nil)

		rr := httptest.NewRecorder()
		data, _ := json.Marshal(&models.User{Name: "John"})
		req, _ := http.NewRequest(http.MethodPut, fmt.Sprintf("/users/%s", userId), bytes.NewBuffer(data))

		server.Serve(rr, req)

		user := models.User{}
		json.NewDecoder(rr.Body).Decode(&user)

		assert.Equal(t, 200, rr.Code)
		assert.Equal(t, userId, user.ID)
		assert.Equal(t, "John", user.Name)
	})

}
