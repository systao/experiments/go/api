package controllers

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"systao.fr/examples/users/internal/models"
	"systao.fr/examples/users/internal/repository"
	"systao.fr/gorest/pkg/gorest"
)

const (
	nameParam = "name"
	idParam   = "id"
)

type controller struct {
	repository repository.UserRepository
}

func (c *controller) greet(context *gin.Context) {
	context.Writer.WriteString(fmt.Sprintf("Hello %s", context.Param(nameParam)))
}

func (c *controller) getUser(context *gin.Context) {
	var user models.User

	if err := context.ShouldBindUri(&user); err != nil {
		context.JSON(400, gin.H{"error": err.Error()})
		return
	}

	u, err := c.repository.FindById(user.ID)

	if err != nil {
		c.handleError(err, context)
		return
	}

	c.writeUser(context, u)
}

func (c *controller) saveUser(context *gin.Context) {
	user, err := c.bindUser(context)
	if err != nil {
		context.JSON(400, gin.H{"error": err.Error()})
		return
	}

	user, serr := c.repository.Save(*user)
	if serr != nil {
		c.handleError(serr, context)
		return
	}

	c.writeUser(context, user)
}

func (*controller) handleError(err error, context *gin.Context) {
	message := err.Error()
	if _, ok := err.(*repository.UserNotFoundError); ok {
		context.JSON(404, gin.H{"error": message})
	} else {
		fmt.Printf("ERR 500 %s", message)
		context.JSON(500, gin.H{"error": message})
	}
}

func (c *controller) bindUser(context *gin.Context) (*models.User, error) {
	var user models.User

	if err := context.ShouldBindJSON(&user); err != nil {
		return nil, err
	}

	if err := context.ShouldBindUri(&user); err != nil {
		return nil, err
	}

	err := c.validateUser(&user, context)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (c *controller) validateUser(user *models.User, context *gin.Context) error {
	if len(user.Name) == 0 {
		return &ValidationError{message: "User name must not be null"}
	}
	return nil
}

func (c *controller) writeUser(context *gin.Context, user *models.User) {
	context.JSON(200, gin.H{"id": user.ID, "name": user.Name})
}

func NewUserController(repository repository.UserRepository) gorest.HTTPController {
	var userController = controller{
		repository: repository,
	}

	return gorest.HTTPController{
		BasePath: "/users",
		Handlers: []gorest.HTTPHandler{
			{
				OnRequest: userController.greet,
				Method:    gorest.Get,
				Path:      fmt.Sprintf("/greet/:%s", nameParam),
				Name:      "users",
			},
			{
				OnRequest: userController.getUser,
				Method:    gorest.Get,
				Path:      fmt.Sprintf("/:%s", idParam),
				Name:      "GetUser",
			},
			{
				OnRequest: userController.saveUser,
				Method:    gorest.Post,
				Path:      "",
				Name:      "CreateUser",
			},
			{
				OnRequest: userController.saveUser,
				Method:    gorest.Put,
				Path:      fmt.Sprintf("/:%s", idParam),
				Name:      "UpdateUser",
			},
		},
	}
}
