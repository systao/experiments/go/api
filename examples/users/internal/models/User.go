package models

type User struct {
	ID   string `json:"id" uri:"id"`
	Name string `json:"name"`
}
