package repository

import (
	"fmt"

	"github.com/google/uuid"
	"systao.fr/examples/users/internal/models"
)

type UserNotFoundError struct {
	ID string
}

type RepositoryStrategy int

const (
	InMemory RepositoryStrategy = iota
)

func (e UserNotFoundError) Error() string {
	return fmt.Sprintf("User not found %s", e.ID)
}

type UserRepository interface {
	Save(u models.User) (*models.User, error)
	FindById(id string) (*models.User, error)
}

type inMemoryUserRepository struct {
	users map[string]models.User
}

func (r *inMemoryUserRepository) FindById(id string) (*models.User, error) {
	for k, u := range r.users {
		if k == id {
			return &u, nil
		}
	}
	return nil, &UserNotFoundError{ID: id}
}

func (r *inMemoryUserRepository) Save(u models.User) (*models.User, error) {
	if len(u.ID) != 0 {
		if _, err := r.FindById(u.ID); err != nil {
			return nil, err
		}
	} else {
		u.ID = uuid.New().String()
	}
	r.users[u.ID] = u
	return &u, nil
}

func NewUserRepository(strategy RepositoryStrategy) UserRepository {
	return &inMemoryUserRepository{
		users: make(map[string]models.User),
	}
}
