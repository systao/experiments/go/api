package main

import (
	"systao.fr/gorest/pkg/gorest"
)

func main() {
	var server = gorest.NewServer(GORestConfig)
	server.Start()
}
