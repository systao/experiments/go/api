package gorest

import (
	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/otel"
)

var Tracer gin.HandlerFunc = func(c *gin.Context) {
	var path = c.FullPath()
	_, span := otel.Tracer(path).Start(c, path)
	defer span.End()
	c.Next()
}
