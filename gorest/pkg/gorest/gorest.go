package gorest

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type HttpMethod int

const (
	Get HttpMethod = iota
	Post
	Put
	Delete
)

func (m HttpMethod) String() string {
	return []string{
		"Get",
		"Post",
		"Put",
		"Delete",
	}[m]
}

func (m HttpMethod) request(engine *gin.Engine) func(string, ...gin.HandlerFunc) gin.IRoutes {
	return map[HttpMethod]func(string, ...gin.HandlerFunc) gin.IRoutes{
		Get:    engine.GET,
		Post:   engine.POST,
		Put:    engine.PUT,
		Delete: engine.DELETE,
	}[m]
}

type HTTPHandler struct {
	OnRequest func(c *gin.Context)
	Method    HttpMethod
	Path      string
	Headers   map[string]string
	Name      string
}

type HTTPController struct {
	BasePath string
	Handlers []HTTPHandler
}

func setHeaders(fn func(context *gin.Context), metadata map[string]string) func(c *gin.Context) {
	return func(c *gin.Context) {
		for k, v := range metadata {
			c.Writer.Header().Set(k, v)
		}
		fn(c)
	}
}

func (c HTTPController) Register(engine *gin.Engine) {
	for _, handler := range c.Handlers {
		if handler.OnRequest != nil {
			fn := handler.OnRequest
			if handler.Headers != nil {
				fn = setHeaders(fn, handler.Headers)
			}
			handler.Method.request(engine)(c.BasePath+handler.Path, fn)
		} else {
			//log incomplete Controller definition
			log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Llongfile)
			log.Printf("Incomplete handler %s definition: missing OnRequest", handler.Name)
		}
	}
}

type Config struct {
	Controllers []HTTPController
	Middlewares []gin.HandlerFunc
	Host        string
	Port        int
}

type server struct {
	config Config
	router *gin.Engine
}

func NewServer(config Config) server {
	s := server{config: config}
	router := gin.Default()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	for _, middleware := range config.Middlewares {
		router.Use(middleware)
	}

	for _, controller := range config.Controllers {
		controller.Register(router)
	}

	s.router = router
	return s
}

func (s *server) Start() {
	config := s.config
	s.router.Run(fmt.Sprintf("%s:%d", config.Host, config.Port))
}

func (s *server) Serve(w http.ResponseWriter, req *http.Request) {
	s.router.ServeHTTP(w, req)
}
